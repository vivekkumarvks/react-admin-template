import "./login.scss";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { TextField } from "@material-ui/core";
import { Button, Stack } from "@mui/material";
import { useAuth } from "./auth";

function Login() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  // const [user, setUser]  = useState('')
  const navigate = useNavigate();
  const auth = useAuth();

  const handleUsername = (event) => {
    setUsername(event.target.value);
  };

  const handlePassword = (event) => {
    setPassword(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    let body = { username, password };

    if (username === "admin" && password === "admin") {
      auth.login(username);
      navigate("/", { replace: true });
    }
  };

  const handleRegister = () => {
    navigate("/register");
  };

  let form = (
    <>
      <form onSubmit={handleSubmit}>
        <Stack className="form-input">
          <TextField
            label="username"
            variant="outlined"
            onChange={handleUsername}
          />
        </Stack>
        <Stack className="form-input">
          <TextField
            label="password"
            variant="outlined"
            type="password"
            onChange={handlePassword}
          />
        </Stack>

        <Stack className="form-input">
          <Button variant="contained" onClick={handleSubmit}>
            Login
          </Button>
        </Stack>
        <Stack className="form-input">
          <Button variant="contained" onClick={handleRegister}>
            Register
          </Button>
        </Stack>
      </form>
    </>
  );
  return (
    <div className="block">
      <div className="login">
        <div className="login-block"></div>
        <div className="login-page">
          <h4>Please Login</h4> {form}
        </div>
        <div className="login-block"></div>
      </div>
    </div>
  );
}

export default Login;
