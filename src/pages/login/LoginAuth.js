import { Navigate, useLocation } from "react-router-dom";

export const LoginAuth = ({ children }) => {
  const location = useLocation();

  if (localStorage.getItem("login")) {
    return <Navigate to="/" state={{ path: location.pathname }} />;
  }
  return children;
};
