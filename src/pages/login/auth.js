import { useState, createContext, useContext } from "react";

const AuthContext = createContext(null);

export const AuthProvider = ({ children }) => {
  const [username, setUsername] = useState(null);

  const login = (username) => {
    setUsername(username);
    localStorage.setItem("login", "done");
  };

  const logout = () => {
    setUsername(null);
    localStorage.removeItem("login");
  };

  return (
    <AuthContext.Provider value={{ username, login, logout }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => {
  return useContext(AuthContext);
};
