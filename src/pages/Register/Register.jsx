import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { TextField } from "@material-ui/core";
import { Button, Stack } from "@mui/material";

function Register() {
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();

  const handleUsername = (event) => {
    setUsername(event.target.value);
  };

  const handlePassword = (event) => {
    setPassword(event.target.value);
  };

  const handleFirstname = (event) => {
    setFirstname(event.target.value);
  };

  const handleLastname = (event) => {
    setLastname(event.target.value);
  };

  const handleEmail = (event) => {
    setEmail(event.target.value);
  };

  const handleRegister = () => {
    navigate("/login");
  };

  let form = (
    <>
      <form>
        <Stack className="form-input">
          <TextField
            label="firstname"
            variant="outlined"
            onChange={handleFirstname}
          />
        </Stack>
        <Stack className="form-input">
          <TextField
            label="lastname"
            variant="outlined"
            onChange={handleLastname}
          />
        </Stack>
        <Stack className="form-input">
          <TextField label="email" variant="outlined" onChange={handleEmail} />
        </Stack>
        <Stack className="form-input">
          <TextField
            label="username"
            variant="outlined"
            onChange={handleUsername}
          />
        </Stack>
        <Stack className="form-input">
          <TextField
            label="password"
            variant="outlined"
            type="password"
            onChange={handlePassword}
          />
        </Stack>

        <Stack className="form-input">
          <Button variant="contained" onClick={handleRegister}>
            Register
          </Button>
        </Stack>
      </form>
    </>
  );

  return (
    <div className="block">
      <div className="login">
        <div className="login-block"></div>
        <div className="login-page">
          <h4>Please Register</h4> {form}
        </div>
        <div className="login-block"></div>
      </div>
    </div>
  );
}

export default Register;
