import React from "react";
import "./fileNotFound.scss";
import { useNavigate } from "react-router-dom";

function FileNotFound() {
  const navigate = useNavigate();

  const handleNavigate = () => {
    navigate("/");
  };

  return (
    <div className="wrapper">
      <div className="height"></div>
      <div className="height">
        <img className="content" src="https://i.imgur.com/qIufhof.png" />
        <div className="info">
          <h3>This page could not be found</h3>
        </div>
        <div className="home" onClick={handleNavigate}>
          go to home, Click me
        </div>
      </div>
      <div className="height"></div>
    </div>
  );
}

export default FileNotFound;
